﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerController : MonoBehaviour {

    private Rigidbody playerRigidbody;
    private bool isFlying = true;

    public float horizontalSpeed;
    public float verticalSpeed;
    public float forwardSpeed;
    private float playerDrag;
    private float playerAngDrag;

    public float rotateSpeed;

    public float timeToWin;
    private float currentTime;
    public TextMeshProUGUI timerText;

    // Use this for initialization
    void Start ()
    {
        playerRigidbody = GetComponent<Rigidbody>();
        playerDrag = this.playerRigidbody.drag;
        playerAngDrag = this.playerRigidbody.angularDrag;
        currentTime = timeToWin;
    }

    /*Update is called once per frame
    void Update ()
    {

    }*/
     
    void FixedUpdate()
    {
        currentTime -= Time.deltaTime;
        timerText.text = "Time Remaining: " + currentTime;
        if (currentTime <= 0)
        {
            SceneManager.LoadScene("Win");
        }

        //If the player is flying...
        if (isFlying)
        {
            //Set the gravity of the player to false.
            this.playerRigidbody.useGravity = false;
        }

        //Controlling Direction:

        float vert = 0;
        float hori = 0;
        bool ignoreAngDrag = false;
        bool ignoreDrag = false;

        //If the player presses "D"
        if (Input.GetKey(KeyCode.D))
        {
            //Rotate the player rightward at the current rotation speed.
            hori = 1;
            ignoreAngDrag = true;
        }

        //If the player presses "A"
        if (Input.GetKey(KeyCode.A))
        {
            //Rotate the player leftward at the current vertical speed.
            hori = -1;
            ignoreAngDrag = true;
        }

        //If the player presses "W"
        if (Input.GetKey(KeyCode.W))
        {
            //Move the player upward at the current rotation speed.
            vert = 1;
            ignoreDrag = true;
        }

        //If the player presses "S"
        if (Input.GetKey(KeyCode.S))
        {
            //Move the player downward at the current vertical speed.
            vert = -1;
            ignoreDrag = true;
        }

        //Vector3 forwardMovement = new Vector3(0.0f, verticalSpeed * vert, forwardSpeed);
        //this.playerRigidbody.velocity = forwardMovement;
        //this.playerRigidbody.transform.Rotate(transform.right, horizontalSpeed * hori, Space.Self);

        Vector3 movement = new Vector3(0.0f, -1 * forwardSpeed, verticalSpeed * vert);
        Vector3 rotation = new Vector3(0.0f, 0.0f, horizontalSpeed * hori);

        //Ignores linear drag when the player is moving, otherwise, applies drag.
        /*if (ignoreDrag)
        {
            this.playerRigidbody.drag = 0;
        }
        else
        {
            this.playerRigidbody.drag = playerDrag;
        }*/

        //Ignores angular drag when the player is moving, otherwise, applies drag.
        if (ignoreAngDrag)
        {
            this.playerRigidbody.angularDrag = 0;
        }
        else
        {
            this.playerRigidbody.angularDrag = playerAngDrag;
        }


        //Vector3 forwardMovement = new Vector3(0.0f, -1 * forwardSpeed, 0.0f);
        this.playerRigidbody.AddRelativeTorque(rotation);
        this.playerRigidbody.inertiaTensor = new Vector3(1, 1, 1);
        this.playerRigidbody.AddRelativeForce(movement);
        //this.playerRigidbody.AddRelativeForce(forwardMovement);

    }
}
