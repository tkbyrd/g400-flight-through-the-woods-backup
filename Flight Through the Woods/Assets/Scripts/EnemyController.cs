﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyController : MonoBehaviour {

    private Rigidbody enemyRigidbody;
    private bool isFlying = true;

    public GameObject player;
    public float horizontalSpeed;
    public float verticalSpeed;
    public float forwardSpeed;
    private float enemyDrag;
    private float enemyAngDrag;

    public float speedUpTimeInterval;
    public float speedUpAmount;
    private float currentTime;

    public float rotateSpeed;

    // Use this for initialization
    void Start()
    {
        enemyRigidbody = GetComponent<Rigidbody>();
        currentTime = speedUpTimeInterval;
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        currentTime -= Time.deltaTime;
        if (currentTime <= 0)
        {
            currentTime = speedUpTimeInterval;
            forwardSpeed += speedUpAmount;
            print(forwardSpeed);
        }
        this.transform.LookAt(player.transform);
        transform.position += transform.forward * forwardSpeed * Time.deltaTime;
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            //Destroy(collision.gameObject);
            SceneManager.LoadScene("Lose");
        }
    }
}
